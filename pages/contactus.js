import Layout from "../components/layout";
import Styles from "../styles/Home.module.css";

const Contactus = () => {
  return (
    <div className={Styles.container}>
      <div className={Styles.title}>Contact US</div>
    </div>
  );
};

Contactus.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default Contactus;
