import Layout from "../components/layout";
import Styles from "../styles/Home.module.css";

const Aboutus = () => {
  return (
    <div className={Styles.container}>
      <div className={Styles.title}>About US</div>
    </div>
  );
};

Aboutus.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default Aboutus;
