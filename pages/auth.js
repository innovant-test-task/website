import Styles from "../styles/Auth.module.css";

import Layout from "../components/layout";
import AuthLayout from "../components/layout/authLayout";

import { useRouter } from "next/router";
import SigninForm from "../components/auth/signinForm";
import RegisterForm from "../components/auth/registerForm";
import AuthTabs from "../components/auth/authTabs";

const Auth = () => {
  const router = useRouter();
  const signin = router.query.selected === "signin";

  return (
    <div className={Styles.container}>
      <AuthTabs isSigninTab={signin}/>
      {signin ? <SigninForm /> : <RegisterForm />}
    </div>
  );
};

Auth.getLayout = function getLayout(page) {
  return (
    <AuthLayout>
      <Layout>{page}</Layout>
    </AuthLayout>
  );
};
export default Auth;
