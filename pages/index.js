import Layout from "../components/layout";
import Styles from "../styles/Home.module.css";

import { useEffect } from "react";
import { useRouter } from "next/router";

const Home = () => {
  const router = useRouter();

  useEffect(() => {
    if (!localStorage.getItem("token")) {
      router.push("/auth?selected=signin");
    }
  }, []);

  return (
    <div className={Styles.container}>
      <div className={Styles.logoback}></div>
      <div className={Styles.logodiv}>
        <div className={`${Styles.logo_shape} ${Styles.logo_shape_1}`}></div>
        <div className={`${Styles.logo_shape} ${Styles.logo_shape_2}`}></div>
      </div>
      <div className={Styles.title}>The Logo Above is Made in Pure CSS</div>
    </div>
  );
};

Home.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default Home;
