import axios from "axios";

import { useState } from "react";

const useRequest = ({ url, method }) => {
  const [backErrors, setBackErrors] = useState(null);

  const doRequest = async (body) => {
    try {
      setBackErrors(null);
      const response = await axios[method](url, body);
      return response.data;
    } catch (error) {
      setBackErrors(
        <div style={{ fontSize: "O.5rem", color: "red" }}>
          {error.response.data.message}
        </div>
      );
    }
  };
  return { doRequest, backErrors };
};

export default useRequest;
