import Styles from "../../styles/Layout.module.css";

const Footer = () => {
  return (
    <footer className={Styles.footer}>
      <div>
        <div className={Styles.logodiv}>
          <div
            className={`${Styles.logo_shape} ${Styles.logo_shape_1}`}
          ></div>
          <div
            className={`${Styles.logo_shape} ${Styles.logo_shape_2}`}
          ></div>
        </div>
        <div>&copy; All Rights Reserved.</div>
      </div>
    </footer>
  );
};

export default Footer;
