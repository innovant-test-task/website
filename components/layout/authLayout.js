import AuthNavbar from "./authNavbar"

const AuthLayout = ({children}) => {
    return <>
        <AuthNavbar/>
        <main>{children}</main>
    </>
}

export default AuthLayout
