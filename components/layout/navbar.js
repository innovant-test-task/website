import Styles from "../../styles/Layout.module.css";

import Link from "next/link";

const Navbar = () => {
  return (
    <nav className={`${Styles.nav} ${Styles.navbar}`}>
      <div className={Styles.mininav}>
        <div className={Styles.logodiv}>
          <div
            className={`${Styles.logo_shape} ${Styles.logo_shape_1}`}
          ></div>
          <div
            className={`${Styles.logo_shape} ${Styles.logo_shape_2}`}
          ></div>
        </div>
        <div className={Styles.navbaritems}>
          <Link href="/">
            <a className={Styles.navbarlink}>Home</a>
          </Link>
          <Link href="/aboutus">
            <a className={Styles.navbarlink}>About us</a>
          </Link>
          <Link href="/contactus">
            <a className={Styles.navbarlink}>Contact us</a>
          </Link>
        </div>
      </div>
      <div className={Styles.usercercle}>Ar</div>
    </nav>
  );
};

export default Navbar;
