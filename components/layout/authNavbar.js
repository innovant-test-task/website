import Styles from "../../styles/Layout.module.css";

import Link from "next/link";

const AuthNavbar = () => {
  return (
    <nav className={`${Styles.nav} ${Styles.authnavbar}`}>
      <Link href="/auth?selected=register">
        <a className={Styles.authlink}>Register</a>
      </Link>
      <Link href="/auth?selected=signin">
        <a className={Styles.authlink}>Sign in</a>
      </Link>
    </nav>
  );
};

export default AuthNavbar;
