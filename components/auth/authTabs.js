import Styles from "../../styles/Auth.module.css";

import { useRouter } from "next/router";

const AuthTabs = ({ isSigninTab }) => {
  const router = useRouter();
  const toggleClick = (e) => {
    router.push(`/auth?selected=${e.target.id}`);
  };

  return (
    <div className={Styles.tabs}>
      <div id="register" onClick={toggleClick}>
        <div
          className={!isSigninTab ? Styles.checkmark : Styles.checkmarkInactive}
        >
          &#10003;
        </div>
        <img src="icons/register.png" alt="register" />
        <p className={Styles.title}>Register</p>
        <p className={Styles.smalltitle}>Browse and find what you need</p>
      </div>
      <div id="signin" onClick={toggleClick}>
        <div style={{display: "flex", justifyContent: "space-between"}}>
          <div></div>
          <div
            className={`${
              isSigninTab ? Styles.checkmark : Styles.checkmarkInactive
            }`}
          >
            &#10003;
          </div>
        </div>
        <img src="icons/signin.png" alt="sign in" />
        <p className={Styles.title}>Sign in</p>
        <p className={Styles.smalltitle}>
          Already have an account, then welcome back.
        </p>
      </div>
    </div>
  );
};

export default AuthTabs;
