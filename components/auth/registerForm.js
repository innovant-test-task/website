import Styles from "../../styles/Auth.module.css";

import { useState } from "react";
import { useRouter } from "next/router";
import useRequest from "../../hooks/use-request";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

const RegisterForm = () => {
  const router = useRouter();

  const [form, setForm] = useState({
    email: "",
    password: "",
    firstname: "",
    lastname: "",
  });

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("First Name is required"),
    lastName: Yup.string().required("Last name is required"),
    email: Yup.string().required("Email is required").email("Email is invalid"),
    password: Yup.string()
      .min(4, "Password must be at least 4 characters")
      .required("Password is required"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Passwords must match")
      .required("Confirm Password is required"),
  });

  const formOptions = { resolver: yupResolver(validationSchema) };

  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  const { doRequest, backErrors } = useRequest({
    url: "http://localhost:5000/api/auth/register",
    method: "post",
  });

  const onChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async (data) => {
    console.log(data);
    const res = await doRequest(data);
    if (res) {
      router.push("/auth?selected=signin");
    }
  };

  return (
    <form className={Styles.formcontainer} onSubmit={handleSubmit(onSubmit)}>
      <div className={Styles.inputsnames}>
        <input
          className={Styles.inputs}
          name="firstName"
          placeholder="First Name*"
          type="text"
          onChange={onChange}
          {...register("firstName")}
          required
        />
        <input
          className={Styles.inputs}
          name="lastName"
          placeholder="Last Name*"
          type="text"
          onChange={onChange}
          {...register("lastName")}
          required
        />
        <div className="invalid-feedback">
          {errors.firstName?.message || errors.lastName?.message}
        </div>
      </div>
      <div>
        <input
          className={Styles.inputs}
          name="email"
          placeholder="Email*"
          type="email"
          onChange={onChange}
          {...register("email")}
          required
        />
        <div className="invalid-feedback">{errors.email?.message}</div>
      </div>
      <div>
        <input
          className={Styles.inputs}
          name="password"
          placeholder="Password*"
          type="password"
          onChange={onChange}
          {...register("password")}
          required
        />
        <div className="invalid-feedback">{errors.password?.message}</div>
      </div>
      <div>
        <input
          className={Styles.inputs}
          name="confirmPassword"
          placeholder="Repeat Password*"
          type="password"
          onChange={onChange}
          {...register("confirmPassword")}
          required
        />
        <div className="invalid-feedback">
          {errors.confirmPassword?.message}
        </div>
      </div>
      {backErrors}
      <button type="submit" className={Styles.button}>
        Submit
      </button>
    </form>
  );
};

export default RegisterForm;
