import Styles from "../../styles/Auth.module.css";

import { useState } from "react";
import useRequest from "../../hooks/use-request";
import { useRouter } from "next/router";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

const SigninForm = () => {
  const router = useRouter();

  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Email is required").email("Email is invalid"),
    password: Yup.string()
      .min(4, "Password must be at least 4 characters")
      .required("Password is required"),
  });

  const formOptions = { resolver: yupResolver(validationSchema) };

  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  const onChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const { doRequest, backErrors } = useRequest({
    url: "http://localhost:5000/api/auth/signin",
    method: "post",
  });

  const onSubmit = async (data) => {
    const res = await doRequest(data);
    if (res) {
      localStorage.setItem("token", res.token);
      router.push("/");
    }
  };

  return (
    <form className={Styles.formcontainer} onSubmit={handleSubmit(onSubmit)}>
      <div>
        <input
          className={Styles.inputs}
          name="email"
          placeholder="Email*"
          type="email"
          onChange={onChange}
          {...register("email")}
          required
        />
        <div className="invalid-feedback">{errors.email?.message}</div>
      </div>
      <div>
        <input
          className={Styles.inputs}
          name="password"
          placeholder="Password*"
          type="password"
          onChange={onChange}
          {...register("password")}
          required
        />
        <div className="invalid-feedback">{errors.password?.message}</div>
      </div>
      {backErrors}
      <button type="submit" className={Styles.button}>
        Submit
      </button>
    </form>
  );
};

export default SigninForm;
